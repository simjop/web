

def setup_routes(app, handler, app_root, static_root):
    router = app.router
    router.add_get('/', handler.index, name='index')
    router.add_get('/auth/login', handler.auth_login, name='auth_login')
    router.add_get('/auth/callback', handler.auth_callback, name='auth_callback')
    router.add_static('/app/', path=app_root, name='app')
    router.add_static('/static/', path=static_root, name='static')
