import socketio
import logging


class WSTestNamespace(socketio.AsyncNamespace):
    async def on_connect(self, sid, environ):
        logging.info('New client connected [%s]', str(sid))

    async def on_disconnect(self, sid):
        logging.info('Client disconnected [%s]', str(sid))
