import aiohttp_jinja2
import logging

from yarl import URL
from aiohttp import web, FormData
from aiohttp_session import get_session
import aiohttp_jinja2

from db.token import create_oauth_token, is_token_valid


def redirect_uri(request):

    result_url = str(request.url.with_path(str(request.app.router['auth_callback'].url_for())))
    if request.app['cfg']['simjop']['https_enabled']:
        result_url = result_url.replace('http://', 'https://')

    return result_url


class SiteHandler:

    def __init__(self):
        self.theme = {
            'background': 'railway-station-4305749_1920.jpg',
        }

    @aiohttp_jinja2.template('index.html')
    async def index(self, request):
        session = await get_session(request)
        logging.info('index | session: %s', str(session))
        return {'theme': self.theme, 'user': session.get('user')}

    async def auth_login(self, request):
        oauth_token = await create_oauth_token(request.app)

        params = {
            'client_id': request.app['cfg']['oauth2']['client_id'],
            'redirect_uri': redirect_uri(request),
            'response_type': 'code',
            'scope': ' '.join(request.app['cfg']['oauth2']['scopes']),
            'prompt': 'consent',  # TODO: remove this if you don't need reapprove authorization
            'state': oauth_token,
        }
        logging.info('auth_login | params: %s', params)

        location = str(URL(request.app['cfg']['oauth2']['authorization_url']).with_query(params))

        return web.HTTPTemporaryRedirect(location=location)

    @aiohttp_jinja2.template('index.html')
    async def auth_callback(self, request):

        logging.info('auth_callback | callback code: %s', request.query['code'])
        logging.info('auth_callback | callback state: %s', request.query['state'])

        is_valid = await is_token_valid(request.app, request.query['state'], 'oauth_state')

        session = await get_session(request)
        return {'theme': self.theme, 'user': session.get('user')}
