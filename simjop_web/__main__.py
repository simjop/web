"""simJOP :: Web

Usage:
  simjop_web init_db [--cfg=<config_file>]
  simjop_web run [--cfg=<config_file>]
  simjop_web (-h | --help)
  simjop_web --version

Options:
  --cfg=<config_file>   Configuration file [default: ./simjop_web.cfg].
  -h --help             Show this screen.
  --version             Show version.
"""

import os
import logging
import configparser
import sys
import asyncio
from docopt import docopt
import aiohttp_jinja2
import jinja2
import socketio
from aiohttp import ClientSession, web
from aiohttp_session import SimpleCookieStorage, setup as session_setup  # TODO: Secure Storage

from db.tables import init_db, db_engine_ctx
from core.routes import setup_routes
from core.views import SiteHandler
from core.sockets import WSTestNamespace

APP_ROOT = os.path.abspath('./simjop_web/app')
STATIC_ROOT = os.path.abspath('./simjop_web/static')
TEMPLATES_ROOT = os.path.abspath('./simjop_web/templates')


def configuration_setup(args):

    cfg = {}

    cfg_parser = configparser.ConfigParser()
    if cfg_parser.read(args['--cfg']):
        print(f"Configuration file [{args['--cfg']}] loaded.")
    else:
        print(f"Configuration file [{args['--cfg']}] doesn't exit.")
        sys.exit(0)

    for section in cfg_parser.sections():
        cfg[section] = {}
        for key in cfg_parser.options(section):
            if key == 'https_enabled':
                cfg[section][key] = cfg_parser.getboolean(section, key)
            else:
                cfg[section][key] = cfg_parser.get(section, key)

    if cfg['simjop']['log_level'] == 'debug':
        logging.basicConfig(level=logging.DEBUG)
    elif cfg['simjop']['log_level'] == 'info':
        logging.basicConfig(level=logging.INFO)
    elif cfg['simjop']['log_level'] == 'warning':
        logging.basicConfig(level=logging.WARNING)
    elif cfg['simjop']['log_level'] == 'error':
        logging.basicConfig(level=logging.ERROR)
    elif cfg['simjop']['log_level'] == 'critical':
        logging.basicConfig(level=logging.CRITICAL)
    else:
        logging.basicConfig(level=logging.INFO)
        logging.warning('Missing simjop/log_level in configuration file [%s]', args['--cfg'])

    db = cfg['db']  # pylint: disable-msg=C0103
    url = f"mariadb+aiomysql://{db['user']}:{db['pass']}@{db['host']}:{db['port']}/{db['name']}"
    cfg['db']['url'] = url

    if args['run']:
        cfg['oauth2']['scopes'] = ['identify']
        cfg['oauth2']['authorization_url'] = 'https://discord.com/api/oauth2/authorize'
        cfg['oauth2']['token_url'] = 'https://discord.com/api/oauth2/token'

    return cfg


async def client_session_ctx(app: web.Application):
    async with ClientSession() as session:
        app['client_session'] = session

    yield


async def init_app(config):
    app = web.Application()

    sio = socketio.AsyncServer(async_mode='aiohttp')
    sio.register_namespace(WSTestNamespace('/test'))
    sio.attach(app)

    app['cfg'] = config

    loader = jinja2.FileSystemLoader(TEMPLATES_ROOT)
    aiohttp_jinja2.setup(app, loader=loader)

    session_setup(app, SimpleCookieStorage())

    app.cleanup_ctx.append(db_engine_ctx)
    app.cleanup_ctx.append(client_session_ctx)

    handler = SiteHandler()
    setup_routes(app, handler, APP_ROOT, STATIC_ROOT)
    return app


def main():

    args = docopt(__doc__, version='0.0.1')
    config = configuration_setup(args)

    if args['init_db']:
        try:
            asyncio.run(init_db(config))
        finally:
            logging.info('Successfully shutdown simjop-web')

    if args['run']:
        app = init_app(config)
        web.run_app(app, port=config['web']['port_number'])


if __name__ == '__main__':
    main()
