import logging
import sys
import datetime as dt
from sqlalchemy.future import select

from db.tables import Token


async def create_token(db_session, token_type, valid_period):

    uid = None

    now = dt.datetime.now()

    token = Token(
        token_type=token_type,
        created_at=now,
        valid_until=now + valid_period,
        is_active=True,
    )

    async with db_session() as session:
        session.add(token)
        await session.flush()
        uid = token.uid
        await session.commit()

    logging.info('vraceny token: %s', uid)

    return uid


async def create_oauth_token(app):
    uid = await create_token(app['db_session'], 'oauth_state', dt.timedelta(minutes=2))
    return uid


async def is_token_valid(app, token, token_type):

    is_valid = False

    async with app['db_session']() as session:
        result = await session.execute(select(Token).where(Token.uid == token))
        token = result.scalars().first()

        now = dt.datetime.now()
        if token.valid_until < now and token_type == token.token_type and token.is_active:
            is_valid = True

    return is_valid
