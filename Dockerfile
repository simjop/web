FROM registry.gitlab.com/simjop/build_image as builder

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY . /app
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

FROM fedora:33
MAINTAINER celestian "petr.celestian@gmail.com"

COPY --from=builder /opt/venv /opt/venv
COPY --from=builder /app/ /app
WORKDIR /app

EXPOSE 80

ENTRYPOINT ["bash", "/app/docker/entrypoint.sh"]
