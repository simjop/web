#!/bin/bash

VIRTUAL_ENV=/opt/venv
python3 -m venv $VIRTUAL_ENV
PATH="$VIRTUAL_ENV/bin:$PATH"

cp /app/docker/simjop_web.cfg.tmpl /app/simjop_web.cfg
sed -i -e "s/<app_log_level>/${APP_LOG_LEVEL}/g" /app/simjop_web.cfg
sed -i -e "s/<web_port_number>/80/g" /app/simjop_web.cfg
sed -i -e "s/<db_host>/${APP_DB_HOST}/g" /app/simjop_web.cfg
sed -i -e "s/<db_port>/${APP_DB_PORT}/g" /app/simjop_web.cfg
sed -i -e "s/<db_user>/${APP_DB_USER}/g" /app/simjop_web.cfg
sed -i -e "s/<db_pass>/${APP_DB_PASSWORD}/g" /app/simjop_web.cfg
sed -i -e "s/<db_name>/${APP_DB_DATABASE}/g" /app/simjop_web.cfg
sed -i -e "s/<oauth2_client_id>/${DISCORD_CLIENT_ID}/g" /app/simjop_web.cfg
sed -i -e "s/<oauth2_client_secret>/${DISCORD_CLIENT_SECRET}/g" /app/simjop_web.cfg

python simjop_web run
