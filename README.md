# The Game of Railway Traffic Control :: Web

## About

The goal of simjop project is create open source game of railway traffic control.
If you want to be in touch with the community, please connect to [discord](https://discord.gg/drcmjBf).

## Configuration

    cp docker/simjop_web.cfg.tmpl simjop_web.cfg

## Run

    python3 -m venv env
    source env/bin/activate
    pip3 install -r requirements.txt


## Development & Contributions

### Local checks

    ./pep8-diff.sh
    pylint simjop_web
